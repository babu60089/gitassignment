########################################################
#
# Before running the following commands,
# please execute the following steps as in _README.docx
# 
# STEPS: 
# 1. Create a BitBucket Account
# 2. Create a Git Repository
# 3. Install Git 
# 4. Clone the Git repository
#
# To clone the Git repository, please execute the following command: 
# CLONE COMMAND: git clone https://mzaman1@bitbucket.org/mzaman1/repo1.git
# Please use your BitBucket ID, instead of �mzaman1�
#
########################################################


# CREATE A FILE AND COMMIT

touch f1.txt 	# Create a new blank file named f1.txt
ls		# Show directory listing (to check if f1.txt is created)

echo "This is f1.txt" >> f1.txt		# Insert some text ("This is f1.txt") in f1.txt
head f1.txt				# View the first ten lines of f1.txt

git add f1.txt	# Stage f1.txt to present repository (repo1) and branch (master)
git status	# Check if the previous command was correctly executed

git commit -m "f1.txt added to master"	# Commit all the staged files in the present branch

git ls-tree -r --name-only master   	# Check if the previous command was correctly executed
# ls-tree: List the contents of a tree object
# -r: Recurse into sub-trees
# --name-only: Show the list of names (only)


# CREATE A BRANCH (B2)

git branch	# Show the name of the present branch

git checkout -b b2  	# Create branch �b2� and start working on �b2�
# Specifying -b causes a new branch to be created 

git branch	# Show the name of the present branch

touch f2.txt	# Create a new blank file named f2.txt
ls		# Show directory listing (to check if f2.txt is created)

echo "This is f2.txt" >> f2.txt		# Insert some text ("This is f2.txt") in f2.txt
head f2.txt				# View the first ten lines of f2.txt

git add f2.txt		# Stage f2.txt to present repository (repo1) and branch (b2)
git status		# Check if the previous command was correctly executed

git commit -m "f2.txt added to b2"	# Commit all the staged files in the present branch
git ls-tree -r --name-only b2		# Check if the previous command was correctly executed



# MERGE BRANCH (B2) WITH MASTER

git branch			# Show the name of the present branch
git checkout master		# Start working on branch �master�
git merge b2 --no-edit  	# Merge b2 to master (present branch). But does not open editor 
git ls-tree -r --name-only master 	# Check if the previous command was correctly executed
 


